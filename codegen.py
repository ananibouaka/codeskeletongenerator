# -*-coding:Latin-1 -*
"""
This module contains the main source code for the codeskeletongenerator program
"""
import os
import csv
from jinja2 import Template, Environment, PackageLoader, select_autoescape
import time

layersPrefix    = {'Applicative':'AP','Adaptative':'AD','Lower layer':'HW'}
renderers       = {}


def create_folder(layer):
    """Creates a layer with specified name"""
    try:
        os.makedirs(layer)
        os.chdir(layer)
        os.makedirs("src")
        os.makedirs("include")
        os.chdir('..')
        print("\nFolder {} has been created".format(layer))
    except OSError:
        if not os.path.isdir(layer):
            raise
        if not os.path.isdir(layer+'/src'):
            raise
        if not os.path.isdir(layer+'/include'):
            raise        


def make_template(renderers):
    """returns the text of the required template file"""
    env = Environment(
    loader=PackageLoader('codegen','templates'),
    #autoescape=select_autoescape(['c', 'h'])
    )
    template = env.get_template(renderers['templateName']+'.tmpl')
    return str(template.render(renderers))
    

def create_file(filename,extension,functions):
    """Creates a source file with specified name from prefix"""
    with open (filename+extension,'w') as file:
        author=os.getlogin( )
        generationDate='{0}'.format(time.strftime('%d/%m/%y',time.localtime()))
        print("File {} has been created".format(filename+extension))
        renderers['templateName']   = 'template'+extension
        renderers['fileName']       = filename
        renderers['functionLists']  = functions
        renderers['author']         = author
        renderers['date']           = generationDate
        file.write(make_template(renderers))
    

def generate_files(confFile):
    """call the necessary methods to creates all the source files"""
    with open(confFile,'r') as configFile:
        configFile.seek(0)      
        reader=csv.DictReader(configFile,delimiter=";")
        prefixComponents=reader.fieldnames
        functions=[]
        #Create folders and files
        for layer,abr in layersPrefix.items():
            create_folder(layer)
            os.chdir(layer+'/src')
            for prefix in  prefixComponents:                    
                filename = abr+'_'+prefix
                #Get the names of the associated functions
                configFile.seek(0)      
                reader=csv.DictReader(configFile,delimiter=";")
                for row in reader:
                    functions.append(row[prefix])
                    
                                
                create_file(filename,'.c',functions)
                functions.clear()    

            #Create header files
            os.chdir('../include')
            for prefix in prefixComponents:
                filename = abr+'_'+prefix
                create_file(filename,'.h',functions)
            os.chdir('../..')   






def main():
    """Main function, must use file config.csv 
    and call all the necessary functions to create folders and files"""
    #os.rmdir('generated_files')
    try:
        os.makedirs('generated_files')
        os.chdir('generated_files')
        generate_files("../config.csv")
        os.chdir('..')
    except OSError:
        if not os.path.isdir('generated_files'):
            raise         
        
    #make_template('template.c')


            


if __name__ == "__main__": main()