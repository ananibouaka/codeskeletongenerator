/*******************************************************************************
Generated source file with codeskeletongenerator 
This file has been generated based on following template file: template.c
*******************************************************************************/
/*******************************************************************************
* Source                : https://gist.github.com/rosterloh/8375961#file-template-c
* Title                 :  
* Filename              : AP_CONF.c 
* Author                : Anani  
* Origin Date           : 25/09/20  
* Version               :   
* Compiler              :   
* Target                :   
* Notes                 :   
*******************************************************************************/
/*************** MODULE REVISION LOG ******************************************
*
*    Date    Software Version    Initials   Description 
*  25/09/20    1.0                  XXX      Module Created.
*
*******************************************************************************/
/** \file AP_CONF.c
 * \brief This module contains the 
 */
/******************************************************************************
* Includes
*******************************************************************************/
/* Inclusion of system and local header files goes here */

#include <stdio.h>
#include <stdlib.h>

/******************************************************************************
* Module Preprocessor Constants
*******************************************************************************/
/* #define and enum statements go here */

/**
 *Doxygen tag for documenting variables and constants
 */
#define   CONSTANT					5

/******************************************************************************
* Module Preprocessor Macros
*******************************************************************************/
/* Macros definitions go here */


/******************************************************************************
* Module Typedefs
*******************************************************************************/
/* Global variables definitions go here */


/******************************************************************************
* Module Variable Definitions
*******************************************************************************/
/* Global variables definitions go here */


/******************************************************************************
* Function Prototypes
*******************************************************************************/
/* Function prototypes for public (external) functions go here */


/******************************************************************************
* Function Definitions
*******************************************************************************/
/******************************************************************************

    /* Function : AP_CONF_getParams()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    AP_CONF_getParams();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void AP_CONF_getParams( void )
    {

    }

    /* Function : AP_CONF_loadConf()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    AP_CONF_loadConf();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void AP_CONF_loadConf( void )
    {

    }

    /* Function : AP_CONF_checkConf()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    AP_CONF_checkConf();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void AP_CONF_checkConf( void )
    {

    }

    /* Function : AP_CONF_activateConf()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    AP_CONF_activateConf();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void AP_CONF_activateConf( void )
    {

    }

/*************** END OF FUNCTIONS ***************************************************************************/