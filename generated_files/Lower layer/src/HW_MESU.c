/*******************************************************************************
Generated source file with codeskeletongenerator 
This file has been generated based on following template file: template.c
*******************************************************************************/
/*******************************************************************************
* Source                : https://gist.github.com/rosterloh/8375961#file-template-c
* Title                 :  
* Filename              : HW_MESU.c 
* Author                : Anani  
* Origin Date           : 25/09/20  
* Version               :   
* Compiler              :   
* Target                :   
* Notes                 :   
*******************************************************************************/
/*************** MODULE REVISION LOG ******************************************
*
*    Date    Software Version    Initials   Description 
*  25/09/20    1.0                  XXX      Module Created.
*
*******************************************************************************/
/** \file HW_MESU.c
 * \brief This module contains the 
 */
/******************************************************************************
* Includes
*******************************************************************************/
/* Inclusion of system and local header files goes here */

#include <stdio.h>
#include <stdlib.h>

/******************************************************************************
* Module Preprocessor Constants
*******************************************************************************/
/* #define and enum statements go here */

/**
 *Doxygen tag for documenting variables and constants
 */
#define   CONSTANT					5

/******************************************************************************
* Module Preprocessor Macros
*******************************************************************************/
/* Macros definitions go here */


/******************************************************************************
* Module Typedefs
*******************************************************************************/
/* Global variables definitions go here */


/******************************************************************************
* Module Variable Definitions
*******************************************************************************/
/* Global variables definitions go here */


/******************************************************************************
* Function Prototypes
*******************************************************************************/
/* Function prototypes for public (external) functions go here */


/******************************************************************************
* Function Definitions
*******************************************************************************/
/******************************************************************************

    /* Function : HW_MESU_sampleData()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    HW_MESU_sampleData();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void HW_MESU_sampleData( void )
    {

    }

    /* Function : HW_MESU_averageData()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    HW_MESU_averageData();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void HW_MESU_averageData( void )
    {

    }

    /* Function : HW_MESU_extractValues()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    HW_MESU_extractValues();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void HW_MESU_extractValues( void )
    {

    }

    /* Function : HW_MESU_formatValues()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    HW_MESU_formatValues();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void HW_MESU_formatValues( void )
    {

    }

/*************** END OF FUNCTIONS ***************************************************************************/