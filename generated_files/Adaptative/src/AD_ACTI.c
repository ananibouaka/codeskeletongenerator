/*******************************************************************************
Generated source file with codeskeletongenerator 
This file has been generated based on following template file: template.c
*******************************************************************************/
/*******************************************************************************
* Source                : https://gist.github.com/rosterloh/8375961#file-template-c
* Title                 :  
* Filename              : AD_ACTI.c 
* Author                : Anani  
* Origin Date           : 25/09/20  
* Version               :   
* Compiler              :   
* Target                :   
* Notes                 :   
*******************************************************************************/
/*************** MODULE REVISION LOG ******************************************
*
*    Date    Software Version    Initials   Description 
*  25/09/20    1.0                  XXX      Module Created.
*
*******************************************************************************/
/** \file AD_ACTI.c
 * \brief This module contains the 
 */
/******************************************************************************
* Includes
*******************************************************************************/
/* Inclusion of system and local header files goes here */

#include <stdio.h>
#include <stdlib.h>

/******************************************************************************
* Module Preprocessor Constants
*******************************************************************************/
/* #define and enum statements go here */

/**
 *Doxygen tag for documenting variables and constants
 */
#define   CONSTANT					5

/******************************************************************************
* Module Preprocessor Macros
*******************************************************************************/
/* Macros definitions go here */


/******************************************************************************
* Module Typedefs
*******************************************************************************/
/* Global variables definitions go here */


/******************************************************************************
* Module Variable Definitions
*******************************************************************************/
/* Global variables definitions go here */


/******************************************************************************
* Function Prototypes
*******************************************************************************/
/* Function prototypes for public (external) functions go here */


/******************************************************************************
* Function Definitions
*******************************************************************************/
/******************************************************************************

    /* Function : AD_ACTI_startMotor()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    AD_ACTI_startMotor();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void AD_ACTI_startMotor( void )
    {

    }

    /* Function : AD_ACTI_runMotor()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    AD_ACTI_runMotor();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void AD_ACTI_runMotor( void )
    {

    }

    /* Function : AD_ACTI_manageSpeed()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    AD_ACTI_manageSpeed();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void AD_ACTI_manageSpeed( void )
    {

    }

    /* Function : AD_ACTI_stopMotor()
    *//** 
    * \section Description Description:
    *
    *  This function is used to 
    *
    * \param  		None.
    *
    * \return 		None.
    *
    * \section Example Example:
    * \code
    *    AD_ACTI_stopMotor();
    * \endcode
    *
    * \see 
    *
    *  ----------------------
    *  - HISTORY OF CHANGES -
    *  ----------------------
    *    Date    Software Version    Initials   Description 
    *  25/09/20    1.0                   XXX      Function Created.
    *
    *******************************************************************************/
    void AD_ACTI_stopMotor( void )
    {

    }

/*************** END OF FUNCTIONS ***************************************************************************/